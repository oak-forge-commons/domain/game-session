package oak.forge.domain.gamesession.data;

import oak.forge.domain.gamesession.command.CreateGameSessionCommand;
import oak.forge.domain.gamesession.event.GameSessionCreatedEvent;
import oak.forge.domain.gamesession.event.failure.CreateGameSessionFailedError;
import org.junit.jupiter.api.Test;
import java.time.Instant;

import static oak.forge.commons.test.impl.AggregateTestUtils.v;
import static org.assertj.core.api.Assertions.assertThat;

class GameSessionCreatorTest extends GameSessionAggregateTestBase {

    private CreateGameSessionCommand createGameSession() {
        return new CreateGameSessionCommand(PLAYER_ID, GAME_SESSION_NAME, Instant.now(), MAX_NUMBER_OF_PLAYERS, GAME_OWNER_NAME);
    }

    @Test
    public void should_create_game_session() {
        fixture.given(this::initEmptyLookups)
                .when(createGameSession())
                .thenExpectEvent(GameSessionCreatedEvent.class)
                .matching(event -> {
                    assertThat(event.getGameSessionName()).isEqualTo(GAME_SESSION_NAME);
                    assertThat(event.getAggregateId()).isNotNull();
                    assertThat(event.getCreatorId()).isEqualTo(PLAYER_ID);
                    assertThat(event.getMaxNumberOfPlayers()).isEqualTo(MAX_NUMBER_OF_PLAYERS);
                    assertThat(event.getPlayerList().size()).isEqualTo(1);
                })
                .thenExpectNoEvents();
    }

    @Test
    public void should_init_state_after_creation_game_session() {
        fixture.given(AGGREGATE_ID)
                .andEvents(gameSessionCreatedEvent())
                .when()
                .thenExpectNoEvents()
                .andStateMatching(gameSessionState -> {
                    assertThat(gameSessionState.getVersion()).isEqualTo(v(1));
                    assertThat(gameSessionState.gameState).isEqualTo(GameState.NOT_RUNNING);
                    assertThat(gameSessionState.getAggregateId()).isEqualTo(AGGREGATE_ID);
                    assertThat(gameSessionState.gameSessionName).isEqualTo(GAME_SESSION_NAME);
                    assertThat(gameSessionState.creatorId).isEqualTo(PLAYER_ID);
                    assertThat(gameSessionState.maximumNumbersOfPlayers).isEqualTo(MAX_NUMBER_OF_PLAYERS);
                    assertThat(gameSessionState.playerList.size()).isEqualTo(0);
                });
    }

    @Test
    public void should_fail_creating_game_session_with_taken_name_and_produce_error_event() {
        fixture.given(this::initGameSessionNameLookup)
                .andMocks(this::initEmptyPlayerInActiveGameLookup)
                .when(createGameSession())
                .thenExpectEvent(CreateGameSessionFailedError.class)
                .matching(error -> {
                            assertThat(error.getGameSessionName()).isEqualTo(GAME_SESSION_NAME);
                            assertThat(error.getMessage()).isEqualTo("Game Session name already exists");
                            assertThat(error.getAggregateId()).isNotNull();
                            assertThat(error.getCreatorId()).isEqualTo(PLAYER_ID);
                        }
                );
    }

    @Test
    public void should_fail_creating_game_session_when_player_is_in_active_game_and_produce_error_event() {
        fixture.given(this::initPlayerInActiveGameLookup)
                .andMocks(this::initEmptyGameSessionNameLookup)
                .when(createGameSession())
                .thenExpectEvent(CreateGameSessionFailedError.class)
                .matching(error -> {
                            assertThat(error.getGameSessionName()).isEqualTo(GAME_SESSION_NAME);
                            assertThat(error.getMessage()).isEqualTo("Player already in active game session");
                            assertThat(error.getAggregateId()).isNotNull();
                            assertThat(error.getCreatorId()).isEqualTo(PLAYER_ID);
                            assertThat(error.getActiveSessionId()).isEqualTo(AGGREGATE_ID.toString());
                        }
                );
    }
}