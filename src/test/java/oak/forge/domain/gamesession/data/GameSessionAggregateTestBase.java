package oak.forge.domain.gamesession.data;

import oak.forge.commons.aggregates.api.aggregate.AggregateLoader;
import oak.forge.commons.bus.MessageBus;
import oak.forge.commons.context.Context;
import oak.forge.commons.eventsourcing.api.eventstore.EventStore;
import oak.forge.commons.test.api.configuration.TestConfiguration;
import oak.forge.domain.gamesession.event.GameSessionCreatedEvent;
import oak.forge.domain.gamesession.lookup.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static oak.forge.commons.test.impl.AggregateFixtureFactory.fixture;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

abstract class GameSessionAggregateTestBase {

    static final Integer MAX_NUMBER_OF_PLAYERS = 5;
    static final List<Player> PLAYER_LIST = new ArrayList<>();
    static final String GAME_SESSION_NAME = "name";
    static final String GAME_OWNER_NAME = "owner-name";
    static final UUID AGGREGATE_ID = UUID.randomUUID();
    static final UUID PLAYER_ID = UUID.randomUUID();
    static final String PLAYER_NAME = "player-name";


    final PlayerInActiveGameLookup playerInActiveGameLookup = mock(PlayerInActiveGameLookup.class);
    final GameSessionNameLookup gameSessionNameLookup = mock(GameSessionNameLookup.class);
    final MessageBus messageBus = mock(MessageBus.class);
    final EventStore eventStore = mock(EventStore.class);
    final AggregateLoader aggregateLoader = mock(AggregateLoader.class);
    final Context context = mock(Context.class);

    final TestConfiguration<GameSessionAggregate.GameSessionState> fixture = fixture(this::createAggregate);

    private GameSessionAggregate createAggregate(){
      return new GameSessionAggregate(messageBus,eventStore,aggregateLoader,gameSessionNameLookup, playerInActiveGameLookup, context);
    }

    GameSessionCreatedEvent gameSessionCreatedEvent(){
        return new GameSessionCreatedEvent(MAX_NUMBER_OF_PLAYERS,PLAYER_LIST,GAME_SESSION_NAME,AGGREGATE_ID,PLAYER_ID);
    }

    void initEmptyGameSessionNameLookup(){
        given(gameSessionNameLookup.get(GAME_SESSION_NAME)).willReturn(Optional.empty());
    }

    void initEmptyPlayerInActiveGameLookup(){
        given(playerInActiveGameLookup.get(PLAYER_ID.toString())).willReturn(Optional.empty());
    }

    void initEmptyLookups(){
        initEmptyGameSessionNameLookup();
        initEmptyPlayerInActiveGameLookup();
    }

    void initGameSessionNameLookup(){
        given(gameSessionNameLookup.get(GAME_SESSION_NAME)).willReturn(Optional.of(AGGREGATE_ID));
    }

    public void initPlayerInActiveGameLookup() {
        given(playerInActiveGameLookup.get(PLAYER_ID.toString())).willReturn(Optional.of(AGGREGATE_ID));
    }

    void initLookups(){
        initGameSessionNameLookup();
        initPlayerInActiveGameLookup();
    }

}
