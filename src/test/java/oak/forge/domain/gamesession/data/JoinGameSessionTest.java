package oak.forge.domain.gamesession.data;

import oak.forge.domain.gamesession.command.JoinGameSessionCommand;
import oak.forge.domain.gamesession.event.GameSessionJoinedEvent;
import oak.forge.domain.gamesession.event.failure.JoinGameSessionFailedError;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;

public class JoinGameSessionTest extends GameSessionAggregateTestBase{

    private JoinGameSessionCommand joinGameSession(){
        return new JoinGameSessionCommand(AGGREGATE_ID, PLAYER_ID, Instant.now(), GAME_SESSION_NAME, PLAYER_NAME);
    }

    @Test
    public void should_join_game_session() {
        fixture.given(this::initEmptyPlayerInActiveGameLookup)
                .andEvents(gameSessionCreatedEvent())
                .when(joinGameSession())
                .thenExpectEvent(GameSessionJoinedEvent.class)
                .matching(event -> {
                            assertThat(event.getGameSessionName()).isEqualTo(GAME_SESSION_NAME);
                            assertThat(event.getAggregateId()).isNotNull();
                            assertThat(event.getPlayerName()).isEqualTo(PLAYER_NAME);
                            assertThat(event.getCreatorId()).isEqualTo(PLAYER_ID);
                        }
                )
                .thenExpectNoEvents();
    }

    @Test
    public void should_fail_joining_game_session_when_player_is_in_active_game_and_produce_error_event(){
        fixture.given(this::initPlayerInActiveGameLookup)
                .andEvents(gameSessionCreatedEvent())
                .when(joinGameSession())
                .thenExpectEvent(JoinGameSessionFailedError.class)
                .matching(error -> {
                    assertThat(error.getGameSessionName()).isEqualTo(GAME_SESSION_NAME);
                    assertThat(error.getMessage()).isEqualTo("Player already in active game session");
                    assertThat(error.getActiveSessionId()).isEqualTo(AGGREGATE_ID.toString());
                    assertThat(error.getCreatorId()).isEqualTo(PLAYER_ID);
                });
    }

    @Test
    public void should_fail_joining_game_when_game_is_running_and_produce_error_event(){
        fixture.given(this::initEmptyPlayerInActiveGameLookup)
                .andState((Consumer<GameSessionAggregate.GameSessionState>) state -> state.setGameState(GameState.RUNNING))
                .when(joinGameSession())
                .thenExpectEvent(JoinGameSessionFailedError.class)
                .matching(error -> {
                    assertThat(error.getMessage()).isEqualTo("Game session started. You can't join the game");
                });

    }
    @Test
    public void should_fail_joining_game_when_game_has_ended_and_produce_error_event(){
        fixture.given(this::initEmptyPlayerInActiveGameLookup)
                .andState((Consumer<GameSessionAggregate.GameSessionState>) state -> state.setGameState(GameState.ENDED))
                .when(joinGameSession())
                .thenExpectEvent(JoinGameSessionFailedError.class)
                .matching(error -> {
                    assertThat(error.getMessage()).isEqualTo("Game session has ended. You can't join the game");
                });
    }

    @Test
    public void should_throw_fail_when_max_number_of_players_is_reached() {
        fixture.given(this::initEmptyPlayerInActiveGameLookup)
                .andState((Consumer<GameSessionAggregate.GameSessionState>) state -> state.setMaximumNumbersOfPlayers(MAX_NUMBER_OF_PLAYERS))
                .andState((Consumer<GameSessionAggregate.GameSessionState>) state -> state.setPlayerList(addPlayersToList(MAX_NUMBER_OF_PLAYERS)))
                .when(joinGameSession())
                .thenExpectEvent(JoinGameSessionFailedError.class)
                .matching(error -> {
                    assertThat(error.getMessage()).isEqualTo("Maximum number of players in game session reached");
                });
    }

    private List<Player> addPlayersToList(int numberOfPlayers){
        List<Player> playerList = new ArrayList<>();
        for (int i = 0; i < numberOfPlayers; i++) {
            playerList.add(new Player(UUID.randomUUID(),PLAYER_NAME+"i",false));
        }
        return playerList;
    }
}
