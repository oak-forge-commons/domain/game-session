package oak.forge.domain.gamesession.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import oak.forge.commons.data.message.Message;
import oak.forge.domain.gamesession.test.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import oak.forge.commons.rabbitmq.DataMapper;
import oak.forge.commons.rabbitmq.RabbitConnectionConfig;
import oak.forge.commons.rabbitmq.SecurityCheck;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Configuration
@EnableConfigurationProperties({RabbitProperties.class})
public class RabbitConfig {

    @Autowired
    private RabbitProperties rabbitProperties;

    @Bean
    Sender sender(RabbitConnectionConfig config) throws IOException, TimeoutException {
        return new Sender(config);
    }

    @Bean
    RabbitConnectionConfig rabbitConnectionConfig() {
        return new RabbitConnectionConfig(rabbitProperties.getUsername(),rabbitProperties.getPassword(),
                rabbitProperties.getHost(),rabbitProperties.getPort(),false);
    }

    @Bean
    SecurityCheck securityCheck() {
        return new SecurityCheck() {
            @Override
            public boolean isTokenValid(String jwt) {
                //todo change later
                return true;
            }
        };
    }

    @Bean
    DataMapper dataMapper() {
        return new DataMapper() {
            private final ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());

            @Override
            public Message getMessageFromString(String json, String className) {
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                Object object = null;
                try {
                    Class<?> clz = Class.forName(className);
                    object = objectMapper.readValue(json, clz);
                } catch (ClassNotFoundException | IOException e) {
                    e.printStackTrace();
                }
                return (Message) object;
            }

            @Override
            public String getStringFromMessage(Message message) throws IOException {
                return objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS).writeValueAsString(message);
            }
        };
    };
}
