package oak.forge.domain.gamesession.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoDatabase;

import oak.forge.commons.aggregates.api.aggregate.AggregateLoader;
import oak.forge.commons.aggregates.impl.aggregate.DefaultAggregateLoader;
import oak.forge.commons.bus.MessageBus;
import oak.forge.commons.context.Context;
import oak.forge.commons.eventsourcing.api.event.EventStreamValidator;
import oak.forge.commons.eventsourcing.api.eventstore.EventStore;
import oak.forge.commons.eventsourcing.api.storage.EventStorage;
import oak.forge.commons.eventsourcing.impl.event.DefaultEventStreamValidator;
import oak.forge.commons.eventsourcing.impl.eventstore.DefaultEventStore;
import oak.forge.commons.mongostorage.api.type.TypeRegistry;
import oak.forge.commons.mongostorage.external.eventstore.storage.MongoEventStorage;
import oak.forge.commons.mongostorage.external.storage.MongoDatabaseProvider;
import oak.forge.commons.mongostorage.impl.mapper.DefaultObjectMapper;
import oak.forge.commons.mongostorage.impl.type.StrictTypeRegistry;
import oak.forge.domain.gamesession.command.CreateGameSessionCommand;
import oak.forge.domain.gamesession.command.JoinGameSessionCommand;
import oak.forge.domain.gamesession.data.GameSessionAggregate;
import oak.forge.domain.gamesession.event.GameSessionCreatedEvent;
import oak.forge.domain.gamesession.event.GameSessionJoinedEvent;
import oak.forge.domain.gamesession.event.failure.CreateGameSessionFailedError;
import oak.forge.domain.gamesession.event.failure.JoinGameSessionFailedError;
import oak.forge.domain.gamesession.lookup.GameSessionNameLookup;
import oak.forge.domain.gamesession.lookup.PlayerInActiveGameLookup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import oak.forge.commons.rabbitmq.DataMapper;
import oak.forge.commons.rabbitmq.RabbitBus;
import oak.forge.commons.rabbitmq.RabbitConnectionConfig;
import oak.forge.commons.rabbitmq.SecurityCheck;

import java.io.IOException;
import java.util.concurrent.TimeoutException;


@Configuration
@EnableConfigurationProperties({RabbitProperties.class})
public class Config {

    public static final String CREATE_GAME_SESSION = "create-game-session";
    public static final String JOIN_GAME_SESSION = "join-game-session";

    enum EventName {
        GAME_SESSION_CREATED,
        CREATE_GAME_SESSION_FAILED,
        GAME_SESSION_JOINED,
        GAME_SESSION_JOINED_FAILED,
    }

    @Autowired
    RabbitProperties rabbitProperties;

    @Bean
    MessageBus messageBus(RabbitConnectionConfig connectionConfig, SecurityCheck securityCheck, DataMapper dataMapper) throws IOException, TimeoutException {
        return new RabbitBus(connectionConfig, securityCheck, dataMapper, "", "", "");
    }


    @Bean
    Context context(GameSessionAggregate gameSessionAggregate) {
        return new Context()
                .registerCommandMapping(CREATE_GAME_SESSION, CreateGameSessionCommand.class, gameSessionAggregate)
                .registerCommandMapping(JOIN_GAME_SESSION, JoinGameSessionCommand.class, gameSessionAggregate);

    }

    @Bean
    GameSessionNameLookup gameSessionLookup() {
        MongoDatabase mongoDatabase = mongoDatabaseProvider().getDatabase("gameDb");
        return new GameSessionNameLookup(mongoDatabase);
    }

    @Bean
    PlayerInActiveGameLookup playerInActiveGameLookup() {
        MongoDatabase mongoDatabase = mongoDatabaseProvider().getDatabase("gameDb");
        return new PlayerInActiveGameLookup(mongoDatabase);
    }

    @Bean
    AggregateLoader aggregateLoader(EventStreamValidator eventStreamValidator) {
        return new DefaultAggregateLoader(eventStreamValidator);
    }

    @Bean
    EventStore eventStore(EventStorage eventStorage, EventStreamValidator eventStreamValidator, MessageBus messageBus) {
        return new DefaultEventStore(eventStorage, eventStreamValidator, messageBus);
    }

    @Bean
    GameSessionAggregate gameSessionAggregate(GameSessionNameLookup gameSessionNameLookup, PlayerInActiveGameLookup playerInActiveGameLookup, AggregateLoader aggregateLoader,
                                              MessageBus messageBus, EventStore eventStore) throws IOException, TimeoutException {
        Context context = new Context();
        return new GameSessionAggregate(messageBus, eventStore, aggregateLoader, gameSessionNameLookup, playerInActiveGameLookup, context);
    }


    @Bean
    MongoDatabaseProvider mongoDatabaseProvider() {
        return new MongoDatabaseProvider("mongodb://localhost:27017");
    }

    @Bean
    TypeRegistry typeRegistry() {
        return new StrictTypeRegistry()
                .register(EventName.GAME_SESSION_CREATED, GameSessionCreatedEvent.class)
                .register(EventName.CREATE_GAME_SESSION_FAILED, CreateGameSessionFailedError.class)
                .register(EventName.GAME_SESSION_JOINED, GameSessionJoinedEvent.class)
                .register(EventName.GAME_SESSION_JOINED_FAILED, JoinGameSessionFailedError.class);
    }

    @Bean
    ObjectMapper objectMapper() {
        return new DefaultObjectMapper();
    }

    @Bean
    MongoEventStorage mongoEventStorage(MongoDatabaseProvider mongoDatabaseProvider, TypeRegistry typeRegistry, ObjectMapper objectMapper) {
        MongoDatabase database = mongoDatabaseProvider.getDatabase("gameDb");
        return new MongoEventStorage(database, objectMapper, typeRegistry);

    }

    @Bean
    EventStorage eventStorage(MongoDatabaseProvider mongoDatabaseProvider, TypeRegistry typeRegistry, ObjectMapper objectMapper) {
        MongoDatabase database = mongoDatabaseProvider.getDatabase("gameDb");
        return new MongoEventStorage(database, objectMapper, typeRegistry);
    }

    @Bean
    EventStreamValidator eventStreamValidator() {
        return new DefaultEventStreamValidator();
    }
}
