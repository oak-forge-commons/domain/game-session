package oak.forge.domain.gamesession.test;

import lombok.Getter;

import java.util.UUID;

@Getter
public class TestJoinDTO {

    private String gameSessionName;
    private String aggregateId;
    private String playerName;
    private String playerId;
}
