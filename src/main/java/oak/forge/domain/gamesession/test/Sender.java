package oak.forge.domain.gamesession.test;

import com.rabbitmq.client.*;
import oak.forge.commons.rabbitmq.RabbitConnectionConfig;
import oak.forge.commons.rabbitmq.SslConfig;
import oak.forge.domain.gamesession.config.RabbitProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Sender {

    private final Channel channel;

    @Autowired
    RabbitProperties rabbitProperties;


    public Sender(RabbitConnectionConfig config) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = getConnectionFactory(config);
        Connection connection = connectionFactory.newConnection();
        channel = connection.createChannel();
    }


    private ConnectionFactory getConnectionFactory(RabbitConnectionConfig config) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername(config.getUsername());
        factory.setPassword(config.getPassword());
        factory.setVirtualHost("/");
        factory.setHost(config.getHost());
        factory.setPort(config.getPort());
        return factory;
    }

    public void sendMessage(String message, String type) throws IOException {
        try {
            channel.basicPublish("", "Commands",
                    new AMQP.BasicProperties().builder()
                            .contentType(type).build(), message.getBytes());
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
