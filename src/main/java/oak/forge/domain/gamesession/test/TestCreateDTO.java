package oak.forge.domain.gamesession.test;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TestCreateDTO {

    private int maxNumberOfPlayers;
    private String gameOwnerName;
    private String gameSessionName;
    private String playerId;

}
