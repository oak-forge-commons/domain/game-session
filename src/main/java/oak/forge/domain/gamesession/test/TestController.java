package oak.forge.domain.gamesession.test;

import oak.forge.domain.gamesession.command.CreateGameSessionCommand;
import oak.forge.domain.gamesession.command.JoinGameSessionCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import oak.forge.commons.rabbitmq.DataMapper;

import java.io.IOException;
import java.time.Instant;
import java.util.UUID;

@RestController
@RequestMapping("/")
public class TestController {

    @Autowired
    DataMapper dataMapper;

    @Autowired
    Sender sender;


    @PostMapping("/create")
    public String create(@RequestBody TestCreateDTO data) throws IOException {
        CreateGameSessionCommand command = new CreateGameSessionCommand(
                UUID.randomUUID(), data.getGameSessionName(), Instant.now(), data.getMaxNumberOfPlayers(), data.getGameOwnerName()
        );
        String message = dataMapper.getStringFromMessage(command);
        sender.sendMessage(message,command.getClass().getTypeName());
        return "post";
    }

    @GetMapping ("/join")
    public String join(@RequestBody TestJoinDTO data) throws IOException {

        UUID aggregateID = UUID.fromString(data.getAggregateId());
        UUID playerId = UUID.fromString(data.getPlayerId());

        JoinGameSessionCommand command = new JoinGameSessionCommand(
                aggregateID, UUID.randomUUID(), Instant.now(), data.getGameSessionName(), data.getPlayerName()
        );
        String message = dataMapper.getStringFromMessage(command);
        sender.sendMessage(message,command.getClass().getTypeName());
        return "post";
    }

}
