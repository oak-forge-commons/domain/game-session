package oak.forge.domain.gamesession.test;

import oak.forge.commons.aggregates.api.aggregate.AggregateLoader;
import oak.forge.commons.context.Context;
import oak.forge.commons.eventsourcing.api.eventstore.EventStore;
import oak.forge.domain.gamesession.data.GameSessionAggregate;
import oak.forge.domain.gamesession.lookup.GameSessionNameLookup;
import oak.forge.domain.gamesession.lookup.PlayerInActiveGameLookup;
import org.springframework.stereotype.Component;
import oak.forge.commons.rabbitmq.DataMapper;
import oak.forge.commons.rabbitmq.RabbitBus;
import oak.forge.commons.rabbitmq.RabbitConnectionConfig;
import oak.forge.commons.rabbitmq.SecurityCheck;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Component
public class Receiver {

    private final RabbitConnectionConfig config;
    private final DataMapper dataMapper;
    private final EventStore eventStore;
    private final AggregateLoader aggregateLoader;
    private final GameSessionNameLookup gameSessionNameLookup;
    private final PlayerInActiveGameLookup playerInActiveGameLookup;
    private final Context context;
    private final SecurityCheck securityCheck;


    public Receiver(RabbitConnectionConfig config, DataMapper dataMapper, EventStore eventStore, AggregateLoader aggregateLoader, GameSessionNameLookup gameSessionNameLookup, PlayerInActiveGameLookup playerInActiveGameLookup, Context context, SecurityCheck securityCheck) throws IOException, TimeoutException {
        this.config = config;
        this.dataMapper = dataMapper;
        this.eventStore = eventStore;
        this.aggregateLoader = aggregateLoader;
        this.gameSessionNameLookup = gameSessionNameLookup;
        this.playerInActiveGameLookup = playerInActiveGameLookup;
        this.context = context;
        this.securityCheck = securityCheck;
        RabbitBus rabbitBus = new RabbitBus(config, this.securityCheck, this.dataMapper,"","","");
        rabbitBus.startCoreService("Commands");
        GameSessionAggregate gameSessionAggregate = new GameSessionAggregate(rabbitBus, this.eventStore,
                this.aggregateLoader, this.gameSessionNameLookup, this.playerInActiveGameLookup, this.context);
    }


}
