package oak.forge.domain.gamesession.lookup;

import com.mongodb.client.MongoDatabase;
import oak.forge.commons.mongostorage.external.storage.MongoSimpleKeyValueStorage;

import java.util.UUID;

public class PlayerInActiveGameLookup extends MongoSimpleKeyValueStorage<String, UUID> {

    private static final String PLAYER_IN_ACTIVE_GAME_LOOKUP = "player-in-active-game-lookup";

    public PlayerInActiveGameLookup(MongoDatabase mongoDatabase) {
        super(mongoDatabase, PLAYER_IN_ACTIVE_GAME_LOOKUP);
    }
}
