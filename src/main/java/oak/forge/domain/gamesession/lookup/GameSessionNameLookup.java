package oak.forge.domain.gamesession.lookup;

import com.mongodb.client.MongoDatabase;
import oak.forge.commons.mongostorage.external.storage.MongoSimpleKeyValueStorage;

import java.util.UUID;

public class GameSessionNameLookup extends MongoSimpleKeyValueStorage<String, UUID> {

	private static final String GAME_SESSION_NAME_LOOKUP = "game-session-name-lookup";

	public GameSessionNameLookup(MongoDatabase mongoDatabase) {
		super(mongoDatabase, GAME_SESSION_NAME_LOOKUP);
	}
}
