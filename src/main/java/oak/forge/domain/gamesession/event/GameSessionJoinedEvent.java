package oak.forge.domain.gamesession.event;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import oak.forge.commons.data.message.Event;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

@Getter
@NoArgsConstructor
public class GameSessionJoinedEvent extends Event implements Serializable {

    private String gameSessionName;
    private String playerName;

    public GameSessionJoinedEvent(UUID aggregateId, UUID creatorId, String gameSessionName, String playerName) {
        super(aggregateId, creatorId, Instant.now());
        this.gameSessionName = gameSessionName;
        this.playerName = playerName;
    }
}
