package oak.forge.domain.gamesession.event;

import lombok.Getter;
import oak.forge.commons.data.message.Event;
import oak.forge.domain.gamesession.data.GameState;
import oak.forge.domain.gamesession.data.Player;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Getter
public class GameSessionCreatedEvent extends Event implements Serializable {

    private final Integer maxNumberOfPlayers;
    private final List<Player> playerList;
    private final String gameSessionName;
    private final GameState gameState;

    public GameSessionCreatedEvent(Integer maxNumberOfPlayers, List<Player> playerList, String gameSessionName,
                                   UUID aggregateId,UUID creatorID) {
        super(aggregateId,creatorID,Instant.now());
        this.maxNumberOfPlayers = maxNumberOfPlayers;
        this.playerList = playerList;
        this.gameSessionName = gameSessionName;
        this.gameState = GameState.NOT_RUNNING;
    }
}
