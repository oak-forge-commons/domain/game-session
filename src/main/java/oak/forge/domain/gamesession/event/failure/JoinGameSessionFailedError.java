package oak.forge.domain.gamesession.event.failure;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import oak.forge.commons.data.message.Error;

import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JoinGameSessionFailedError extends Error {

    private String message;
    private String gameSessionName;
    private String activeSessionId;

    public JoinGameSessionFailedError(String message, String gameSessionName) {
        this.message = message;
        this.gameSessionName = gameSessionName;
    }
}
