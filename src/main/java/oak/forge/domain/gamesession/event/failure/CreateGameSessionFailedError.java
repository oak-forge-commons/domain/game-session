package oak.forge.domain.gamesession.event.failure;

import lombok.*;
import oak.forge.commons.data.message.Error;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateGameSessionFailedError extends Error {

    private String message;
    private String gameSessionName;
    private String activeSessionId;

    public CreateGameSessionFailedError(String message, String gameSessionName) {
        this.message = message;
        this.gameSessionName = gameSessionName;
    }
}
