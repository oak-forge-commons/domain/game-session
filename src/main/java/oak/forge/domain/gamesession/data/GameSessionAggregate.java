package oak.forge.domain.gamesession.data;

import lombok.Getter;
import lombok.Setter;
import oak.forge.commons.aggregates.api.aggregate.AggregateLoader;
import oak.forge.commons.aggregates.impl.aggregate.AbstractAggregateRoot;
import oak.forge.commons.bus.MessageBus;
import oak.forge.commons.context.Context;
import oak.forge.commons.data.aggregate.AggregateState;
import oak.forge.commons.eventsourcing.api.eventstore.EventStore;
import oak.forge.domain.gamesession.command.CreateGameSessionCommand;
import oak.forge.domain.gamesession.command.JoinGameSessionCommand;
import oak.forge.domain.gamesession.event.GameSessionCreatedEvent;
import oak.forge.domain.gamesession.event.GameSessionJoinedEvent;
import oak.forge.domain.gamesession.event.failure.CreateGameSessionFailedError;
import oak.forge.domain.gamesession.event.failure.JoinGameSessionFailedError;
import oak.forge.domain.gamesession.lookup.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class GameSessionAggregate extends AbstractAggregateRoot<GameSessionAggregate.GameSessionState> {

    Logger logger = LoggerFactory.getLogger(GameSessionAggregate.class);

@Getter
@Setter
    public static class GameSessionState extends AggregateState {

        public GameState gameState;
        public List<Player> playerList;
        public UUID creatorId;
        public int maximumNumbersOfPlayers;
        public String gameSessionName;

        public GameSessionState(UUID aggregateId) {
            super(aggregateId);
            this.gameState = GameState.NOT_RUNNING;
        }
    }

    public GameSessionAggregate(MessageBus messageBus, EventStore eventStore, AggregateLoader aggregateLoader,
                                GameSessionNameLookup gameSessionNameLookup, PlayerInActiveGameLookup playerInActiveGameLookup
            , Context context) {
        super(messageBus, eventStore, aggregateLoader, context);
        GameSessionCreator gameSessionCreator = new GameSessionCreator(gameSessionNameLookup, playerInActiveGameLookup);
        GameSessionConnector gameSessionConnector = new GameSessionConnector(playerInActiveGameLookup);

        registerCommandHandler(CreateGameSessionCommand.class, gameSessionCreator::createGameSession);
        registerCommandValidator(CreateGameSessionCommand.class, gameSessionCreator::validateCreateGameSession);
        registerEventHandler(GameSessionCreatedEvent.class, gameSessionCreator::onGameSessionCreated);
        registerEventHandler(CreateGameSessionFailedError.class, gameSessionCreator::onCreateGameSessionFailed);

        registerCommandHandler(JoinGameSessionCommand.class, gameSessionConnector::joinGameSession);
        registerCommandValidator(JoinGameSessionCommand.class, gameSessionConnector::validateJoinGameSession);
        registerEventHandler(GameSessionJoinedEvent.class, gameSessionConnector::onGameSessionJoined);
        registerEventHandler(JoinGameSessionFailedError.class, gameSessionConnector::onJoinGameSessionFailed);
    }

    @Override
    public GameSessionState initState(UUID aggregateId) {
        return new GameSessionState(aggregateId);
    }


}
