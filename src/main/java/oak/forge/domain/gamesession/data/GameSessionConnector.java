package oak.forge.domain.gamesession.data;

import lombok.RequiredArgsConstructor;
import oak.forge.commons.data.message.Command;
import oak.forge.commons.data.message.Error;
import oak.forge.commons.data.message.Event;
import oak.forge.domain.gamesession.command.JoinGameSessionCommand;
import oak.forge.domain.gamesession.event.GameSessionJoinedEvent;
import oak.forge.domain.gamesession.event.failure.JoinGameSessionFailedError;
import oak.forge.domain.gamesession.lookup.PlayerInActiveGameLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@RequiredArgsConstructor
public class GameSessionConnector {

    Logger logger = LoggerFactory.getLogger(GameSessionConnector.class);

    private final PlayerInActiveGameLookup playerInActiveGameLookup;

    public <C extends Command> List<Event> joinGameSession(JoinGameSessionCommand command, GameSessionAggregate.GameSessionState gameSessionState) {
        UUID aggregateId = command.getAggregateId();
        UUID playerId = command.getCreatorId();
        String gameSessionName = command.getGameSessionName();
        String playerName = command.getPlayerName();

        GameSessionJoinedEvent gameSessionJoinedEvent = new GameSessionJoinedEvent(aggregateId, playerId, gameSessionName, playerName);
        playerInActiveGameLookup.put(playerId.toString(),aggregateId);

        return Collections.singletonList(gameSessionJoinedEvent);
    }

    public <E extends Error> List<Error> validateJoinGameSession(JoinGameSessionCommand command, GameSessionAggregate.GameSessionState state) {
        String playerId = command.getCreatorId().toString();

        if (playerInActiveGameLookup.get(playerId).isPresent()) {
            String activeSessionId = playerInActiveGameLookup.get(playerId).get().toString();
            String message = "Player already in active game session";
            return Collections.singletonList(joinGameSessionFail(command, activeSessionId, message));
        }

       if(state.getGameState().equals(GameState.RUNNING)){
           String message = "Game session started. You can't join the game";
           return Collections.singletonList(joinGameSessionFail(command, message));
       }

       if(state.getGameState().equals(GameState.ENDED)){
           String message = "Game session has ended. You can't join the game";
           return Collections.singletonList(joinGameSessionFail(command, message));
       }

       if((state.getMaximumNumbersOfPlayers()) <= state.getPlayerList().size()){
           String message = "Maximum number of players in game session reached";
           return Collections.singletonList(joinGameSessionFail(command, message));
       }

       return new ArrayList<>();
    }

    private JoinGameSessionFailedError joinGameSessionFail(JoinGameSessionCommand command, String activeSessionId, String message) {
        JoinGameSessionFailedError error = new JoinGameSessionFailedError(message, command.getGameSessionName(),activeSessionId);
        error.setAggregateId(command.getAggregateId());
        error.setCreatorId(command.getCreatorId());
        return error;
    }

    private JoinGameSessionFailedError joinGameSessionFail(JoinGameSessionCommand command, String message){
        JoinGameSessionFailedError error = new JoinGameSessionFailedError(message, command.getGameSessionName());
        error.setAggregateId(command.getAggregateId());
        error.setCreatorId(command.getCreatorId());
        return error;
    }

    public <E extends Event> void onGameSessionJoined(GameSessionJoinedEvent event, GameSessionAggregate.GameSessionState gameSessionState) {
        Player player = new Player(event.getCreatorId(), event.getPlayerName(), false);
        gameSessionState.playerList.add(player);
    }

    public void onJoinGameSessionFailed(JoinGameSessionFailedError error,GameSessionAggregate.GameSessionState gameSessionState) {

    }
}
