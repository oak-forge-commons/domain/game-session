package oak.forge.domain.gamesession.data;

import oak.forge.commons.data.message.Error;
import oak.forge.commons.data.message.Event;
import oak.forge.domain.gamesession.command.CreateGameSessionCommand;
import oak.forge.domain.gamesession.event.GameSessionCreatedEvent;
import oak.forge.domain.gamesession.event.failure.CreateGameSessionFailedError;
import oak.forge.domain.gamesession.lookup.GameSessionNameLookup;
import oak.forge.domain.gamesession.lookup.PlayerInActiveGameLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public final class GameSessionCreator {

    private static final Logger logger = LoggerFactory.getLogger(GameSessionCreator.class);

    private final GameSessionNameLookup gameSessionNameLookup;
    private final PlayerInActiveGameLookup playerInActiveGameLookup;

    public GameSessionCreator(GameSessionNameLookup gameSessionNameLookup, PlayerInActiveGameLookup playerInActiveGameLookup) {
        this.gameSessionNameLookup = gameSessionNameLookup;
        this.playerInActiveGameLookup = playerInActiveGameLookup;
    }

    public List<Event> createGameSession(CreateGameSessionCommand command, GameSessionAggregate.GameSessionState gameSessionState) {
        List<Player> playersList = new LinkedList<>();
        Player player = new Player(command.getCreatorId(), command.getGameOwnerName(), true);
        playersList.add(player);

        UUID aggregateId = UUID.randomUUID();
        UUID creatorId = command.getCreatorId();
        Integer maximumNumberOfPlayers = command.getMaxNumberOfPlayers();
        String gameSessionName = command.getGameSessionName();

        GameSessionCreatedEvent gameSessionCreatedEvent = new GameSessionCreatedEvent(
                maximumNumberOfPlayers,playersList,gameSessionName,aggregateId,creatorId);

        List<Event> gameSessionCreatedEvents = Collections.singletonList(gameSessionCreatedEvent);

        gameSessionNameLookup.put(gameSessionName,aggregateId);
        playerInActiveGameLookup.put(player.getPlayerId().toString(),aggregateId);

        return gameSessionCreatedEvents;
    }

    public void onGameSessionCreated(GameSessionCreatedEvent event, GameSessionAggregate.GameSessionState gameSessionState) {
        gameSessionState.playerList = event.getPlayerList();
        gameSessionState.creatorId = event.getCreatorId();
        gameSessionState.gameSessionName = event.getGameSessionName();
        gameSessionState.maximumNumbersOfPlayers = event.getMaxNumberOfPlayers();
        gameSessionState.gameState = event.getGameState();
    }

    public List<Error> validateCreateGameSession(CreateGameSessionCommand command, GameSessionAggregate.GameSessionState state){
        if (gameSessionNameLookup.get(command.getGameSessionName()).isPresent()) {
            return Collections.singletonList(createGameSessionFail(command,
                    "Game Session name already exists"));
        }
        String playerId = command.getCreatorId().toString();
        if (playerInActiveGameLookup.get(playerId).isPresent()) {
            String activeSessionId = playerInActiveGameLookup.get(playerId).get().toString();
            return Collections.singletonList(createGameSessionFail(command,
                    "Player already in active game session",activeSessionId));
        }
        return new ArrayList<>();
    }


    public <E extends Event> void onCreateGameSessionFailed(CreateGameSessionFailedError event, GameSessionAggregate.GameSessionState gameSessionState) {

    }

    private CreateGameSessionFailedError createGameSessionFail(CreateGameSessionCommand command, String message){
        CreateGameSessionFailedError createGameSessionFailedEvent = new CreateGameSessionFailedError(message, command.getGameSessionName());
        createGameSessionFailedEvent.setCreatorId(command.getCreatorId());
        createGameSessionFailedEvent.setAggregateId(UUID.randomUUID());
        return createGameSessionFailedEvent;
    }

    private CreateGameSessionFailedError createGameSessionFail(CreateGameSessionCommand command, String message, String activeSessionId){
        CreateGameSessionFailedError createGameSessionFailedEvent = new CreateGameSessionFailedError(message, command.getGameSessionName(),activeSessionId);
        createGameSessionFailedEvent.setCreatorId(command.getCreatorId());
        createGameSessionFailedEvent.setAggregateId(UUID.randomUUID());
        return createGameSessionFailedEvent;
    }

}


