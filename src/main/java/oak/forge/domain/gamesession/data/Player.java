package oak.forge.domain.gamesession.data;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Getter
@RequiredArgsConstructor
public class Player {
    private final UUID playerId;
    private final String playerName;
    private final boolean playerGameOwner;

}
