package oak.forge.domain.gamesession.data;

public enum GameState {
    NOT_RUNNING,
    RUNNING,
    ENDED
}
