package oak.forge.domain.gamesession.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import oak.forge.commons.data.message.Command;

import java.time.Instant;
import java.util.UUID;

@Getter
@NoArgsConstructor
public class CreateGameSessionCommand extends Command {

    private int maxNumberOfPlayers;
    private String gameOwnerName;
    private String gameSessionName;

    public CreateGameSessionCommand(UUID playerId, String gameSessionName, Instant timeStamp, int maxNumberOfPlayers, String gameOwnerName) {
        super(null, playerId, timeStamp);
        this.maxNumberOfPlayers = maxNumberOfPlayers;
        this.gameOwnerName = gameOwnerName;
        this.gameSessionName = gameSessionName;
        withName("create-game-session");
    }
}
