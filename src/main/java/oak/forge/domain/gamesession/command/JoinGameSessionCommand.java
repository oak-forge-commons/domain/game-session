package oak.forge.domain.gamesession.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import oak.forge.commons.data.message.Command;

import java.time.Instant;
import java.util.UUID;

@Getter
@NoArgsConstructor
public class JoinGameSessionCommand extends Command {

    private String gameSessionName;
    private String playerName;

    public JoinGameSessionCommand(UUID aggregateId, UUID creatorId, Instant timestamp, String gameSessionName, String playerName) {
        super(aggregateId, creatorId, timestamp);
        this.gameSessionName = gameSessionName;
        this.playerName = playerName;
        withName("join-game-session");
    }
}
